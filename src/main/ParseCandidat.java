package main;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class ParseCandidat {
    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            // JSON file to Java Object
            Candidat candidat = mapper.readValue(new File("fichiers/candidatComplet.json"), Candidat.class);

            // Affichage simple
            // System.out.println(candidat);

            // Affichage soigné
            String joliCandidat = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(candidat);
            System.out.println(joliCandidat);
        } catch (IOException error) {
            System.err.println(error);
        }

        // Ajout d'un candidat
        System.out.println("Ajout d'un candidat");
        Candidat candidat2 = returnCandidat();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File("fichiers/creeCandidat.json"), candidat2);
            System.out.println("Candidat crée avec success");
        } catch (IOException error) {
            System.out.println("Erreur lors de la création du candidat");
        }
    }

    public static Candidat returnCandidat() {
        Candidat candidat = new Candidat();
        Scanner scanner = new Scanner(System.in);

        // Saisie nom
        System.out.print("Nom : ");
        candidat.setNom(scanner.nextLine());

        // Saisie prénom
        System.out.print("Prénom : ");
        candidat.setPrenom(scanner.nextLine());

        // Saisie Age
        System.out.print("Age : ");
        candidat.setAge(scanner.nextInt());
        scanner.nextLine();

        //Saisie loisirs
        System.out.println("Loisirs (saisie quitter pour passer à la suite) : ");
        boolean continu = true;
        ArrayList<String> loisirs = new ArrayList<>();
        do {
            System.out.print(" - ");
            String input = scanner.nextLine();
            if(input.equalsIgnoreCase("quitter")) {
                continu = false;
            }
            else {
                if (input.length() > 0) loisirs.add(input);
            }
        } while (continu);
        candidat.setLoisirs(loisirs);

        // Saisie Salaire
        System.out.println("Salaires (saisie quitter pour passer à la suite) : ");
        continu = true;
        Map<String, BigDecimal> salaires = new TreeMap<>();
        do {
            System.out.print("Année : ");
            String annee = scanner.nextLine();
            if(annee.equalsIgnoreCase("quitter")) {
                continu = false;
            }
            else {
                System.out.print("Salaire " + annee + " : ");
                BigDecimal salaire = scanner.nextBigDecimal();
                scanner.nextLine();
                salaires.put(annee, salaire);
            }
        } while (continu);
        candidat.setSalaire(salaires);

        //Saisie compétence
        System.out.println("Compétence (saisie quitter pour passer à la suite) : ");
        continu = true;
        ArrayList<String> competences = new ArrayList<>();
        do {
            System.out.print(" - ");
            String input = scanner.nextLine();
            if(input.equalsIgnoreCase("quitter")) {
                continu = false;
            }
            else {
                if (input.length() > 0) competences.add(input);
            }
        } while (continu);
        candidat.setCompetences(competences);

        // Saisie Expérience
        System.out.println("Expériences (saisie quitter pour passer à la suite) : ");
        continu = true;
        Map<String, Integer> experiences = new TreeMap<>();
        do {
            System.out.print("Expérience : ");
            String experience = scanner.nextLine();
            if(experience.equalsIgnoreCase("quitter")) {
                continu = false;
            }
            else {
                System.out.print("Score " + experience + " : ");
                Integer score = scanner.nextInt();
                scanner.nextLine();
                experiences.put(experience, score);
            }
        } while (continu);
        candidat.setExperiences(experiences);

        // Saisie Disponible
        continu = true;
        do {
            System.out.print("Disponible (oui/non) : ");
            String input = scanner.nextLine().toLowerCase();

            if(input.equals("oui")) {
                candidat.setDisponible(true);
                continu = false;
            }
            else if(input.equals("non")) {
                candidat.setDisponible(false);
                continu = false;
            }
            else System.out.println("Valeur saisie incorrect");
        } while (continu);

        return candidat;
    }
}
