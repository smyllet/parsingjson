package main;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class Candidat {
    String nom, prenom;
    int age;
    List<String> loisirs;
    Map<String, BigDecimal> salaire;
    List<String> competences;
    Map<String, Integer> experiences;
    boolean disponible;

    public Candidat() {

    }

    public Candidat(String nom, String prenom, int age, List<String> loisirs, Map<String, BigDecimal> salaire, List<String> competences, Map<String, Integer> experiences, boolean disponible) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.loisirs = loisirs;
        this.salaire = salaire;
        this.competences = competences;
        this.experiences = experiences;
        this.disponible = disponible;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<String> getLoisirs() {
        return loisirs;
    }

    public void setLoisirs(List<String> loisirs) {
        this.loisirs = loisirs;
    }

    public Map<String, BigDecimal> getSalaire() {
        return salaire;
    }

    public void setSalaire(Map<String, BigDecimal> salaire) {
        this.salaire = salaire;
    }

    public List<String> getCompetences() {
        return competences;
    }

    public void setCompetences(List<String> competences) {
        this.competences = competences;
    }

    public Map<String, Integer> getExperiences() {
        return experiences;
    }

    public void setExperiences(Map<String, Integer> experiences) {
        this.experiences = experiences;
    }

    public boolean getDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    @Override
    public String toString() {
        return "Candidat{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", age=" + age +
                ", loisirs=" + loisirs +
                ", salaire=" + salaire +
                ", competences=" + competences +
                ", experiences=" + experiences +
                ", disponible=" + disponible +
                '}';
    }
}